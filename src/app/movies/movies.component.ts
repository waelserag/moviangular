import { Component, OnInit } from '@angular/core';

import { ApiService } from '../api.service';

declare var $: any;

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
})
export class MoviesComponent implements OnInit {
  klma: any = '';

  obj = {
    name: 'merit',
    age: 26,
  };

  currentPage = 1;

  changePage(eventInfo: any) {
    console.log(eventInfo.target.innerHTML); //page number
    this.currentPage = eventInfo.target.innerHTML;




    this._ApiService.getMovies(this.currentPage).subscribe((x) => {
      this.moviesarray = x.results; //overright
    });




  }

  getNext() {
    this.currentPage++;

    this._ApiService.getMovies(this.currentPage).subscribe((x) => {
      this.moviesarray = x.results; //overright
    });
  }
  getPrev() {
    this.currentPage--;

    this._ApiService.getMovies(this.currentPage).subscribe((x) => {
      this.moviesarray = x.results; //overright
    });
  }

  moviesarray: any;

  imgSrc = 'https://image.tmdb.org/t/p/w500';

  constructor(private _ApiService: ApiService) {}

  //constructor  >>> calling automatic
  ngOnInit(): void {
    // $('#test').hide(5000);

    $(document).ready(function () {
      $('.owl-carousel').owlCarousel();
    });

    this._ApiService.getMovies(this.currentPage).subscribe((x) => {
      console.log(x.results);

      this.moviesarray = x.results;
    });
  }
}
