import { Component, OnInit } from '@angular/core';

import { FormGroup ,  FormControl  , Validators} from '@angular/forms';

import { AuthonService } from '../authon.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  ln_Validation = 'registerForm.controls.last_name.invalid   &&  registerForm.controls.last_name.touched';

  // fn = new FormControl()
  // lname = new FormControl()

  registerForm = new FormGroup({

     first_name : new FormControl(''  , [Validators.required , Validators.minLength(3) , Validators.maxLength(20)]),
     last_name : new FormControl( null , [Validators.required ,  Validators.minLength(3) ]),

     age:new FormControl( null , [Validators.required , Validators.min(10) , Validators.max(80)]),
     email:new FormControl(null  ,[Validators.required, Validators.email]),
     password :new FormControl( null , [Validators.required , Validators.pattern(/^[1-9]/)])
  })




  msg:any;

  getData(x:any){

  
     if(x.valid){

      console.log(x.value )

      this._AuthonService.doSignUp(x.value ).subscribe( (feedback)=>{

        console.log(feedback)

        this.msg = feedback.message;

        if( feedback.message == 'success'){

          this._Router.navigate(['/login'])

        }

      } )
    
     }
   
  }

  constructor( private  _AuthonService:AuthonService , private _Router:Router ) { }

  ngOnInit(): void {
  }

} 


// template driven form  >>simple


//reactive form >> compil testing , sclale 

