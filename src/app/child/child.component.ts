import { Component, Input, OnInit, Output , EventEmitter } from '@angular/core';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  productName:any;
  productModel:any;

//decorator
  @Input()  container:any;

  @Output()  productDetails = new EventEmitter();


  setProduct(){

    this.productDetails.emit({
      productName:this.productName , model :this.productModel
    })
  }



  constructor() { }

  ngOnInit(): void {
  }

}
