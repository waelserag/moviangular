import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AuthonGuard } from './authon.guard';
import { BlogComponent } from './blog/blog.component';
import { ProductsComponent } from './dashboard/products/products.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MoviesComponent } from './movies/movies.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:'',redirectTo:'/register' , pathMatch:'full'},


  {path:'home',component:HomeComponent , canActivate:[AuthonGuard ]},


  {path:'about',component:AboutComponent , canActivate:[AuthonGuard ]},


  {path:'blog',component:BlogComponent , canActivate:[AuthonGuard ]},


  {path:'movies',component:MoviesComponent , canActivate:[AuthonGuard ]},


  {path:'products', loadChildren:()=>{ return import('../app/dashboard/dashboard.module').
  then( (m)=>{ return m.DashboardModule} )  }},


  // {path:'products',component:ProductsComponent},
  {path:'register',component:RegisterComponent},



  {path:'login',component:LoginComponent},



  {path:'moviedetails/:movieId/:moviename',component:MovieDetailsComponent  , canActivate:[AuthonGuard ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes , {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }





// pipe search //production 

// sass xd hosting



