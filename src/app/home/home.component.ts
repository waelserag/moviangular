import { Component, OnInit } from '@angular/core';
import { User } from '../user';


import{DataService} from '../data.service'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usersArray:any= [];

  constructor(_DataService:DataService) { 
    this.usersArray = _DataService.users;

  }

  ngOnInit(): void {
  }

}



