import { Injectable } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { User } from './user';






//decorator  >> angular


@Injectable({

  providedIn:'root'   //all project

})



// service >> class >> shared data 


// dependency injection 

export class DataService {


  users:User[] = [

    {name:'merit',age:10,gender:'female',dob:'22/99/9999',salary:445454 , welcome:function(){
      console.log('hello' + this.name)
    }},

    {name:'ali',age:20,gender:'male',dob:'10/99/9999',salary:445454 , welcome:function(){
      alert('hello' + this.name  )

    }},
 

    {name:'ahmed',age:20,gender:'male',dob:'10/99/9999',salary:445454 ,welcome:function(){
      alert(this.dob )

    }},
 
    {name:'omar',age:20,gender:'male',dob:'10/99/9999',salary:445454 ,welcome:function(){
      alert(this.dob )

    }},
 
    {name:'omnia',age:20,gender:'male',dob:'10/99/9999',salary:445454 ,welcome:function(){
      alert(this.dob )

    }},
 


 

 

  ]

  age= 60;


  constructor() { }
}




