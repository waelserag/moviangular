import { Component, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../api.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {


  // dynamic routing

  movieObject:any;

  constructor(  private _ApiService : ApiService  ,  private  _ActivatedRoute :ActivatedRoute  ) {

    console.log('hello from constructor')
   }

  ngOnInit(): void {

    
    console.log('hello from ng on init')

  
    console.log( this._ActivatedRoute.snapshot.params)


    this._ApiService.getMoviedetails(this._ActivatedRoute.snapshot.params.movieId).subscribe( (data)=>{
      console.log(data)

      this.movieObject = data;
    }  )



  }



// @Input()  @Output()

  ngOnChanges(): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

    console.log('ng on change')
    
  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

    console.log('ng on destroy')
    
  }

}


// life cycle hook of component  craetion  change   destroy
