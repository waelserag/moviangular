import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthonService } from '../authon.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLogin:any; //false



  logOut(){
    this._router.navigate(['/login'])

    this._AuthonService.checkLogin.next(false);

  }

  constructor( private _AuthonService:AuthonService , private _router:Router ) { }


  ngOnInit(): void {

    // this.isLogin =   this._AuthonService.checkLogin;

    this._AuthonService.checkLogin.subscribe( (x)=>{

      console.log(x);

      this.isLogin = x;


      console.log('behavior subject')

    } )





  }

}
