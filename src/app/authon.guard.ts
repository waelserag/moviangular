import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthonService } from './authon.service';

@Injectable({
  providedIn: 'root',
})
export class AuthonGuard implements CanActivate {
  
  constructor(private _AuthonService: AuthonService   , private _Router:Router) {}

  canActivate() {
    if (this._AuthonService.checkLogin.getValue() == true) {
      //el user 3ml login

      return true;
    }
    else{
      //msh 3aml login 

      this._Router.navigate(['/login'])


      return false
    }
  }



}
