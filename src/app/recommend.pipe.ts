import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recommend'
})
export class RecommendPipe implements PipeTransform {

  transform(p:any): any{
    return 'recommend ' + p ;
  }

}
