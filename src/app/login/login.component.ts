import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthonService } from '../authon.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  msg: any;

  login(form: any) {
    if (form.valid) {
      console.log(form.value);

      this._AuthonService.signIn(form.value).subscribe((x:any) => {
        console.log(x);

        this.msg = x;

        if (x.message == 'success') {
          console.log('navigate')
          this._Router.navigate(['/movies']);

         // this._AuthonService.checkLogin = true;

         this._AuthonService.checkLogin.next(true) ; //assign value to be sub

          // console.log(  this._AuthonService.checkLogin)
        }
      });
    }
  }

  constructor(private _AuthonService: AuthonService, private _Router: Router) {}

  ngOnInit(): void {}
}
