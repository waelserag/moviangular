import { Injectable } from '@angular/core';

import {HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(private _HttpClient:HttpClient ) {



   }  


  apiLink = 'https://api.themoviedb.org/3/movie/now_playing?api_key=afed2bdc759c185496dcd94a60b71d77&language=en-US&page=1';


  getMovies(pageNum:any):Observable<any>{

    let res =   this._HttpClient.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=afed2bdc759c185496dcd94a60b71d77&language=en-US&page=${pageNum}`);

     return res;  //>>//observable  class rxjs library
  }


  getMoviedetails(id:any):Observable<any>{

   return  this._HttpClient.get(`https://api.themoviedb.org/3/movie/${id}?api_key=afed2bdc759c185496dcd94a60b71d77&language=en-US`)

  }

  




  
}



//observable >>> class in RXJS library   //dealing with  http reques  



//fetch() >> retrun promise


//single movies

// authon 

// lazyloading 

// input output

// sass 

// adobe xd