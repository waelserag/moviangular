import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthonService {

  constructor( private   _HttpClient:HttpClient   ) { }


 // checkLogin = false; //boolean


 checkLogin = new BehaviorSubject(false);

 

 //behavior subject() >> type observable >>> subscribe()




  // get()  >> method get 

  // post() >>> method post 
  // delete()  >> method delete

doSignUp(user:any):Observable<any>{

 return this._HttpClient.post('https://route-egypt-api.herokuapp.com/signup' , user);
}



signIn(obj:any){
 return this._HttpClient.post('https://route-egypt-api.herokuapp.com/signin',obj);

}


}
