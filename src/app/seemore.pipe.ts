import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'seemore'
})
export class SeemorePipe implements PipeTransform {

  transform(str:any , stopPoint:any ): any{

    return str.substring(0,stopPoint) + '...';
  }

}
